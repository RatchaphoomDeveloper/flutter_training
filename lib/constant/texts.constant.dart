import 'package:flutter/material.dart';
import 'package:flutter_training/constant/colors.constant.dart';

class AppText {
  static final r10 = _TextColor(
    fontSize: 10,
    fontWeight: FontWeight.normal,
  );

  static final r12 = _TextColor(
    fontSize: 12,
    fontWeight: FontWeight.normal,
  );

  static final r14 = _TextColor(
    fontSize: 14,
    fontWeight: FontWeight.normal,
  );

  static final r16 = _TextColor(
    fontSize: 16,
    fontWeight: FontWeight.normal,
  );

  static final r18 = _TextColor(
    fontSize: 18,
    fontWeight: FontWeight.normal,
  );

  static final r20 = _TextColor(
    fontSize: 20,
    fontWeight: FontWeight.normal,
  );

  static final r24 = _TextColor(
    fontSize: 24,
    fontWeight: FontWeight.normal,
  );

  static final r32 = _TextColor(
    fontSize: 32,
    fontWeight: FontWeight.normal,
  );
  static final b10 = _TextColor(
    fontSize: 10,
    fontWeight: FontWeight.w700,
  );
  static final b12 = _TextColor(
    fontSize: 12,
    fontWeight: FontWeight.w700,
  );

  static final b14 = _TextColor(
    fontSize: 14,
    fontWeight: FontWeight.w700,
  );
  static final b16 = _TextColor(
    fontSize: 16,
    fontWeight: FontWeight.w700,
  );

  static final b18 = _TextColor(
    fontSize: 18,
    fontWeight: FontWeight.w700,
  );

  static final b20 = _TextColor(
    fontSize: 20,
    fontWeight: FontWeight.w700,
  );

  static final b22 = _TextColor(
    fontSize: 22,
    fontWeight: FontWeight.w700,
  );

  static final b24 = _TextColor(
    fontSize: 24,
    fontWeight: FontWeight.w700,
  );

  static final b32 = _TextColor(
    fontSize: 32,
    fontWeight: FontWeight.w700,
  );

  static final l16 = _TextColor(
    fontSize: 16,
    fontWeight: FontWeight.w300,
  );

  static final l14 = _TextColor(
    fontSize: 14,
    fontWeight: FontWeight.w300,
  );

  static final l10 = _TextColor(
    fontSize: 10,
    fontWeight: FontWeight.w300,
  );
  static final l12 = _TextColor(
    fontSize: 12,
    fontWeight: FontWeight.w300,
  );

  static final l18 = _TextColor(
    fontSize: 18,
    fontWeight: FontWeight.w300,
  );

  static final l20 = _TextColor(
    fontSize: 20,
    fontWeight: FontWeight.w300,
  );

  static final l24 = _TextColor(
    fontSize: 24,
    fontWeight: FontWeight.w300,
  );

  static final l32 = _TextColor(
    fontSize: 32,
    fontWeight: FontWeight.w300,
  );
}


class _TextColor {
  _TextColor({required this.fontSize, required this.fontWeight});
  final double fontSize;
  final FontWeight fontWeight;

  TextStyle get white => TextStyle(
      fontSize: fontSize, fontWeight: fontWeight, color: AppColors.white);

  TextStyle get black => TextStyle(
      fontWeight: fontWeight, fontSize: fontSize, color: AppColors.black);

  TextStyle get lightBlack => TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: AppColors.lightBlack,
      );

  TextStyle get superLightBlack => TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: AppColors.superLightBlack,
      );

  TextStyle get red => TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: AppColors.red,
      );

  TextStyle get green => TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: AppColors.green,
      );

  TextStyle get superLightGreen => TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: AppColors.superLightGreen,
      );

  TextStyle get lightGreen => TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: AppColors.lightGreen,
      );

  TextStyle get grey => TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: AppColors.grey,
      );

  TextStyle get superLightGrey => TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: AppColors.superLightGrey,
      );

  TextStyle get lightGrey => TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: AppColors.lightGrey,
      );
}
