import 'package:flutter/material.dart';

class AppColors {
  static Color superLightBlack = Colors.black54;
  static Color lightBlack = Colors.black87;
  static Color black = Colors.black;

  static Color white = Colors.white;

  static Color superLightRed = Colors.red.shade300;
  static Color lightRed = Colors.red.shade400;
  static Color red = Colors.red;

  static Color superLightGreen = Colors.green.shade300;
  static Color lightGreen = Colors.green.shade400;
  static Color green = Colors.green;

  static Color grey = Colors.grey;

  static Color superLightGrey = Colors.grey.shade300;
  static Color lightGrey = Colors.grey.shade400;
}
