import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class AppConfigOption {
  static String get fileName => kReleaseMode ? ".env.production" : ".env";
  static String get getMode => dotenv.env['MODE'] ?? "Can't Load Mode";
}