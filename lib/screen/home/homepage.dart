import 'package:flutter/material.dart';
import 'package:flutter_training/component/button_component/buttoncomponent.dart';
import 'package:flutter_training/function/auth/auth.function.dart';

import '../../component/input_component/inputcomponent.dart';

class Homepage extends StatefulWidget {
  const Homepage({Key? key, required this.title, required this.text})
      : super(key: key);
  final String title;
  final String text;
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: SingleChildScrollView(
        child: Column(
          children: [
            // ElevatedButton(onPressed: () => null, child: const Text("login"),style:  ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20)),),
            Inputcomponent(
              controller: _username,
              color: Colors.black,
              hintText: 'Username',
              required: true,
              obscureText: false,
              obscuringCharacter: '*',
              readOnly: false,
              fontSize: 16,
              textColor: Colors.black,
              label: 'Username',
            ),
            const SizedBox(
              height: 20,
            ),
            Inputcomponent(
              controller: _password,
              color: Colors.black,
              hintText: 'Password',
              required: true,
              obscureText: true,
              obscuringCharacter: '*',
              readOnly: false,
              fontSize: 16,
              textColor: Colors.black,
              label: 'Password',
            ),
            const SizedBox(
              height: 20,
            ),
            Buttoncomponent(
                  buttonType: 1,
                  handlerFunction: () => {
                    AuthFunction.handlerLogin(
                        context, _username.text, _password.text)
                  },
                  buttonColor: Colors.amber,
                  label: 'Login',
                  buttonWidth: double.infinity,
                ),
                   Buttoncomponent(
                  buttonType: 1,
                  handlerFunction: () => {
                    AuthFunction.handlerLogin(
                        context, _username.text, _password.text)
                  },
                  buttonColor: Colors.red,
                  label: 'Register',
                   buttonWidth:  double.infinity,
                ),

              
             
          ],
        ),
      )),
    );
  }
}
