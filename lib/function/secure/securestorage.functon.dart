import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorageKey {
  static const String user = 'user';
}

class SecureStorageFunction {
  static const _storage = FlutterSecureStorage();

  static Future<void> setUser(String user) async => 
  await _storage.write(key: SecureStorageKey.user, value: user);

  static Future<Map<String,dynamic>?> getUser() async{
    String? val = await _storage.read(key: SecureStorageKey.user);
    if(val == null) return null;
    try {
      return json.decode(val);
    } catch (e) {
      throw new Exception(e);
    }
  }
}