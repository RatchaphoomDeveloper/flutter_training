import 'package:flutter/material.dart';
import 'package:flutter_training/function/auth/auth.function.dart';
import 'package:flutter_training/screen/home/homematerpage.dart';

class Buttoncomponent extends StatefulWidget {
  const Buttoncomponent(
      {Key? key, required this.buttonType, required this.handlerFunction, this.buttonColor, this.label, this.buttonWidth})
      : super(key: key);
  final int buttonType;
  final Color? buttonColor;
  final String? label;
  final double? buttonWidth;
  final void Function() handlerFunction;

  @override
  _ButtoncomponentState createState() => _ButtoncomponentState();
}

class _ButtoncomponentState extends State<Buttoncomponent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 10,left: 10),
      child: _buttonStyle(context),
    );
  }

  Widget _buttonStyle(BuildContext context) {
    return SizedBox(
      width: widget.buttonWidth,
      child: ElevatedButton(
        onPressed: widget.handlerFunction,
        child:  Text(widget.label ?? ''),
        style: ElevatedButton.styleFrom(backgroundColor: widget.buttonColor),
      ),
    );
  }
}
