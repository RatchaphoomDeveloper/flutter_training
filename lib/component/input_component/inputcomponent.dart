import 'package:flutter/material.dart';
import 'package:flutter_training/constant/texts.constant.dart';

class Inputcomponent extends StatefulWidget {
  const Inputcomponent(
      {Key? key,
      this.hintText = '',
      this.required = false,
      this.obscureText = false,
      this.obscuringCharacter,
      this.readOnly = false,
      this.fontSize,
      this.textColor,
      this.color,
      this.errorText,
      this.label, this.validator, this.controller
      })
      : super(key: key);
  final String? hintText;
  final bool? required;
  final bool? obscureText;
  final String? obscuringCharacter;
  final bool? readOnly;
  final double? fontSize;
  final Color? textColor;
  final Color? color;
  final String? errorText;
  final String? label;
  final String? Function(String? text)? validator;
  final TextEditingController? controller;
  @override
  _InputcomponentState createState() => _InputcomponentState();
}

class _InputcomponentState extends State<Inputcomponent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10,right: 10),
      child: _view(),
    );
  }

  Widget _view() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(widget.label ?? '',style: TextStyle(fontSize: 16),),
        SizedBox(height: 7,),
        _textField()
      ],
    );
  }

  Widget _textField() {
    return TextFormField(
      autocorrect: false,
      obscuringCharacter: widget.obscuringCharacter ?? '*',
      obscureText: widget.obscureText ?? false,
      readOnly: widget.readOnly ?? false,
      controller: widget.controller,
      style: TextStyle(
          fontSize: widget.fontSize, color: widget.textColor ?? widget.color),
      validator: widget.validator,
      decoration: InputDecoration(
          hintText: widget.hintText,
          border: const OutlineInputBorder(),
          errorMaxLines: 1,
          errorStyle: AppText.r14.red,
          errorText: widget.errorText,
          contentPadding:
              const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          errorBorder: OutlineInputBorder(
              borderSide: const BorderSide(width: 2, color: Colors.red),
              borderRadius: BorderRadius.circular(8)),
          focusedErrorBorder: OutlineInputBorder(
              borderSide: const BorderSide(width: 2, color: Colors.red),
              borderRadius: BorderRadius.circular(8))),
    );
  }
}
